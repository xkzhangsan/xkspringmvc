# xkspringmvc

#### 项目介绍
      springmvc+mybatis+druid+mysql的简单框架，方便易用。没有集成太多其他的功能，适合初学者，快速运行起来，看到效果。
      v1.0.4 完成了框架的集成和增删改成示例。
      v2.0.1 完成了用户注册登录和登录拦截功能，密码使用MD5加密保存。
      v2.0.2 将加入简单分页功能。

#### 软件架构
软件架构说明,springmvc+mybatis+druid+mysql组成的简单框架。


#### 安装教程

1. 先创建mysql数据库：xkspringmvc，再根据xkspringmvc/src/main/resources/mapper/mysql-db.sql建表语句建表，并新增数据。
2. clone项目，导入IDE。
3. 启动项目，v1.0.4及以前版本，访问http://localhost:8080/xkspringmvc/user/list， 查看显示结果，包括简单的增删改查示例。
4. 访问http://localhost:8080/xkspringmvc/druid/index.html，查看druid监控页面。
3. 启动项目，v2.0.1及以后版本，访问http://localhost:8080/xkspringmvc/， 登录系统。

#### 使用说明

1. jsp页面如果需要使用EL表达式，需要添加<%@ page isELIgnored="false" %>，才能正常显示。

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

创建项目过程中，参考了一些网上的教程，感谢他们！

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)