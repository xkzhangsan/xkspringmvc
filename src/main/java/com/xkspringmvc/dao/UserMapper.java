package com.xkspringmvc.dao;

import java.util.List;

import com.xkspringmvc.model.User;

public interface UserMapper {

	public List<User> getUserList();
	
    User selectByUsername(String username);
    
    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);	
}
