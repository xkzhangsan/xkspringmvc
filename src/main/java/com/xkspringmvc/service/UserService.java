package com.xkspringmvc.service;

import java.util.List;

import com.xkspringmvc.model.User;

public interface UserService {

	public List<User> getUserList();
	
	/**
	 * 根据用户名查询用户
	 * @param username
	 * @return
	 */
    User selectByUsername(String username);
	
    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);	
}
