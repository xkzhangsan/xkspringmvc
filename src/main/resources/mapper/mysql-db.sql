CREATE TABLE `user` (
  `id` bigint(20) NOT NULL auto_increment COMMENT '用户ID',
  `name` varchar(50) NOT NULL COMMENT '用户名',
  `age` int(3) default NULL COMMENT '用户年龄',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL COMMENT '密码',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_username` USING BTREE (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户表';

