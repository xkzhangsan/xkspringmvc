<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>xkspringmvc</title>
<base href="<%=basePath%>" target="_self"></base>
</head>
<body>
	<center>
		${error_msg}
		<hr>
		<form action="<%=basePath%>/user/updatePassword" method="post">
		<input type="hidden" name="id" value="${user.id}"><br>
		原密码：<input type="password" name="password"><br>
		新密码：<input type="password" name="rePassword"><br>		
		<input type="submit" value="提交">
		</form>
	</center>
</body>
</html>