<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>xkspringmvc</title>
<base href="<%=basePath%>" target="_self"></base>
<script src="<%=basePath%>/static/js/jquery-1.11.1.min.js"></script>
<link href="<%=basePath%>/static/css/style.css" rel="stylesheet"
	type="text/css" />
</head>
<body>
	<div class="middle">
		当前登录用户为：${loginUser.username}<br> <a
			href="<%=basePath%>/user/editUser">新增用户</a>&nbsp;<a
			href="<%=basePath%>/user/list">查询所有用户</a>&nbsp;<a
			href="<%=basePath%>/user/toUpdatePassword?id=${loginUser.id}">修改密码</a>&nbsp;<a
			href="<%=basePath%>/user/logout">退出登录</a>
		<hr>
		<c:forEach items="${userList}" var="user">
			<tr>
				<td>${user.id}</td>
				<td>${user.name}</td>
				<td>${user.age}</td>
				<td>${user.username}</td>
				<td><a href="<%=basePath%>/user/editUser?id=${user.id}">修改用户</a><a
					href="<%=basePath%>/user/deleteUser?id=${user.id}">删除用户</a></td>
			</tr>
			<br>
		</c:forEach>

		<table class="gridtable" style="width: 100%;">
			<thead>
				<tr>
					<th>ID</th>
					<th>姓名</th>
					<th>年龄</th>
					<th>用户名</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${pageInfo.list}" var="user">
					<tr>
						<td>${user.id}</td>
						<td>${user.name}</td>
						<td>${user.age}</td>
						<td>${user.username}</td>
						<td><a href="<%=basePath%>/user/editUser?id=${user.id}">修改用户</a><a
							href="<%=basePath%>/user/deleteUser?id=${user.id}">删除用户</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<table class="gridtable" style="width: 100%; text-align: center;">
			<tr>
				<c:if test="${pageInfo.hasPreviousPage}">
					<td><a
						href="<%=basePath%>/user/list?page=${pageInfo.prePage}&rows=${pageInfo.pageSize}">前一页</a>
					</td>
				</c:if>
				<c:forEach items="${pageInfo.navigatepageNums}" var="nav">
					<c:if test="${nav == pageInfo.pageNum}">
						<td style="font-weight: bold;">${nav}</td>
					</c:if>
					<c:if test="${nav != pageInfo.pageNum}">
						<td><a
							href="<%=basePath%>/user/list?page=${nav}&rows=${pageInfo.pageSize}">${nav}</a>
						</td>
					</c:if>
				</c:forEach>
				<c:if test="${pageInfo.hasNextPage}">
					<td><a
						href="<%=basePath%>/user/list?page=${pageInfo.nextPage}&rows=${pageInfo.pageSize}">下一页</a>
					</td>
				</c:if>
			</tr>
		</table>
	</div>
</body>
</html>