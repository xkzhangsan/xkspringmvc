package com.xkspringmvc.test;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xkspringmvc.model.User;
import com.xkspringmvc.service.UserService;

public class UserServiceTest extends SpringTestCase{

   @Autowired
   private UserService userDao;

   @Test
   public void testGetUserList(){
       List<User> userList = userDao.getUserList();
       for (User user : userList) {
           System.out.println(user);
       }
   }

}
